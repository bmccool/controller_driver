#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/slab.h> /* for kmalloc */
MODULE_LICENSE("Dual BSD/GPL");

int controller_major =   0;
int controller_minor =   0;
int controller_nr_devs = 1;

struct controller_dev * controller_devices;

struct controller_dev {
	int data;           /* Pointer to first quantum set */
	struct cdev cdev;   /* Char device structure        */
};

static void hello_exit(void);

static int hello_init(void)
{
    int result, i;
    dev_t dev = 0;

/*
 * Get a range of minor numbers to work with, asking for a dynamic
 * major unless directed otherwise at load time.
 */
    if (controller_major) 
        {
            dev = MKDEV(controller_major, controller_minor);
            result = register_chrdev_region(dev, controller_nr_devs, "controller");
        } 
        else 
        {
            result = alloc_chrdev_region(&dev, controller_minor, controller_nr_devs,"controller");
            controller_major = MAJOR(dev);
        }
        if (result < 0) 
        {
            printk(KERN_WARNING "scull: can't get major %d\n", controller_major);
            return result;
        }

/* 
 * allocate the devices -- we can't have them static, as the number
 * can be specified at load time
 */
    controller_devices = kmalloc(controller_nr_devs * sizeof(struct controller_dev), GFP_KERNEL);
    if (!controller_devices) 
    {
        result = -ENOMEM;
        goto fail;  /* Make this more graceful */
    }
    memset(controller_devices, 0, controller_nr_devs * sizeof(struct controller_dev));

    /* Initialize each device. */
    for (i = 0; i < controller_nr_devs; i++) 
    {
        controller_devices[i].data = 0;
        //controller_setup_cdev(&controller_devices[i], i);
    }

    return 0; /* succeed */

    fail:
        printk(KERN_WARNING "controller: Failed to initialize");
        hello_exit();
        return result;
}


/*
 * The cleanup function is used to handle initialization failures as well.
 * Thefore, it must be careful to work correctly even if some of the items
 * have not been initialized
 */
static void hello_exit(void)
{
	dev_t devno = MKDEV(controller_major, controller_minor);

	/* Get rid of our char dev entries */
	if (controller_devices) 
	{
		kfree(controller_devices);
	}

	/* cleanup_module is never called if registering failed */
	unregister_chrdev_region(devno, controller_nr_devs);

}

module_init(hello_init);
module_exit(hello_exit);
